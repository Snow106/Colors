//plays the sound
if (buttonSound = true)
{
    audio_play_sound(snd_Button, 10, false);
    buttonSound = false;
}
if (hitSound = true)
{
    audio_play_sound(snd_BlockHit, 10, false);
    hitSound = false;
}
if (mergeSound = true)
{
    audio_play_sound(snd_Merge, 10, false);
    mergeSound = false;
}
if (swipeSound = true)
{
    audio_play_sound(snd_Swipe, 10, false);
    swipeSound = false;
}
if (levelCompleteSound = true)
{
    audio_play_sound(snd_LevelComplete, 10, false);
    levelCompleteSound = false;
}
if (menuSound = true)
{
    audio_play_sound(snd_MenuOpen, 10, false);
    menuSound = false;
}

//Start Background Music
if (room = Levels || room = welcome)
{   
    if (!audio_is_playing(snd_BGMusic))
    {
        audio_play_sound(snd_BGMusic, 1, true);
    }    
}

//Stop Background Music
if (room != Levels && room != welcome)
{   
    if (audio_is_playing(snd_BGMusic))
    {
        audio_stop_sound(snd_BGMusic);
    }    
}
