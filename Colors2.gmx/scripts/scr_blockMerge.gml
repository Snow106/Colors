//argument0 = [1 = they merge] [2 = get's absorbed] [3 = this instance  doesn't do anything] [4= same color merge]
//argument1 = what's merging


closestAnchor = instance_nearest(x,y,obj_anchor);
redBlue = 10;
redYellow = 11;
blueYellow = 12;
whiteBlack = 13

if (argument[1] = redBlue)
{
    merge = obj_purple
}
if (argument[1] = redYellow)
{
    merge = obj_orange
}
if (argument[1] = blueYellow)
{
    merge = obj_green
}

if (argument[1] = whiteBlack)
{
    merge = obj_fill
}


//Blocks Merge
if (argument[0] = 1)
{
    if (speed = 0)
    {
        instance_create(closestAnchor.x,closestAnchor.y, merge);
        instance_destroy();
        obj_soundManager.mergeSound = true;
    }
    else
    {
        instance_destroy();
    }
}

//Block Gets Absorbed
if (argument[0] = 2)
{
        instance_destroy();
        obj_soundManager.mergeSound = true;
    
}

//Nothing Happens
if(argument[0] = 3)
{
    speed = 0

    x = closestAnchor.x;
    y = closestAnchor.y;
    obj_soundManager.hitSound = true;
}

//The one that's not moving gets destroyed 
if argument[0] = 4
{
    if speed !=0 
    {
    obj_soundManager.mergeSound = true;
    instance_destroy();
    }
}
