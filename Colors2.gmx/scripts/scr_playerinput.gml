/*if (obj_BlockParent.speed = 0)
{   
    canSwipe = true
}
else
{
    canSwipe = false
}*/
allSpeed = 0;
with (obj_BlockParent) {obj_overlord.allSpeed += speed;}

if ( allSpeed = 0 )
{
    canSwipe = true;
}
else
{
    canSwipe = false;
}

    //swipe   
if (swipe && SwipeOn && canSwipe)
{
    movingBlock = instance_nearest(x_start, y_start, obj_BlockParent);
    canSwipe = false;  
    if ((point_distance(mouse_x,mouse_y,x_start,y_start) > 100) && (point_distance(x_start,y_start,movingBlock.x,movingBlock.y) <= 200)) //player has to move finger 50 pixels for swipe to work
      {
        obj_soundManager.swipeSound = true;
        
        dir = point_direction(x_start,y_start, mouse_x,mouse_y,);
        if ( ((dir <= 45) && (dir >=0)) || ((dir <= 360) && (dir>315)) )//right
        {
            with (movingBlock.object_index)
                {
                        motion_add(0, swipeSpeed);
                }
                swipes += 1;
        }
        else if ((dir > 45) && (dir <= 135)) //up
        {
           with (movingBlock.object_index)
            {
                    motion_add(90, swipeSpeed);
            }
              swipes += 1;
        }
        else if ((dir > 135) && (dir <= 225)) //left
        {
            with (movingBlock.object_index)
            {
                    motion_add(180, swipeSpeed);
            }
              swipes += 1;
        }
        else if ((dir > 225) && (dir <= 315)) //down
        {
          with (movingBlock.object_index)
            {
                    motion_add(270, swipeSpeed);
            }
            swipes += 1;
        }
        swipe = false;
        
        //pause = false;
        // alarm[1] = 90;
      }
    else if (mouse_check_button_released(mb_left) == true) //player releases mouse before reaching threshold
    {
      swipe = false;
    }
}

/*swipeUp = || keyboard_check_pressed(vk_up)
swipeLeft = ||keyboard_check_pressed(vk_left) 
swipeRight = || keyboard_check_pressed(vk_right) 
swipeDown = || keyboard_check_pressed(vk_down)*/


//Go to next level
if ( alarm0 = 0 && instance_number(obj_targetRed) <= instance_number(obj_red) && instance_number(obj_targetBlue) <= instance_number(obj_blue)  && instance_number(obj_targetYellow) <= instance_number(obj_yellow)  && instance_number(obj_targetOrange) <= instance_number(obj_orange)  && instance_number(obj_targetPurple) <= instance_number(obj_purple)  && instance_number(obj_targetGreen) <= instance_number(obj_green)  && instance_number(obj_targetBlack) <= instance_number(obj_black)  && instance_number(obj_targetWhite) <= instance_number(obj_white)) 
{
obj_soundManager.levelCompleteSound = true;
alarm[0] = 60
alarm0 = 1
}

