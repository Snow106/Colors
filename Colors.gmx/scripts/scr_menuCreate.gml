#define scr_menuCreate

if (menuOn = false)
{
    menuOn = true;
    obj_overlord.SwipeOn = false;
    obj_soundManager.buttonSound = true;
    instance_create(0, 0, obj_pause);
    instance_create(0, 0, obj_menuExit);
    instance_create(720, 256+(192*0), obj_colorBlind);
    instance_create(864, 256+(192*0), obj_theme);
    instance_create(720, 256+(192*1), obj_resume);
    instance_create(720, 256+(192*2), obj_levels);
    instance_create(720, 256+(192*3), obj_exit);
    instance_create(720, 1584, obj_logo2);
}

else if (menuOn = true)
{
menuOn = false;
obj_soundManager.buttonSound = true;
obj_overlord.SwipeOn = true;
with (obj_exit)
{
    instance_destroy();
}
with (obj_levels)
{
    instance_destroy();
}

with (obj_pause)
{
    instance_destroy();
}
with (obj_resume)
{
    instance_destroy();
}
with (obj_menuExit)
{
    instance_destroy();
}
with (obj_colorBlind)
{
    instance_destroy();
}
with (obj_theme)
{
    instance_destroy();
}
with (obj_logo2)
{
    instance_destroy();
}
}


#define script9
