//plays the sound
if (buttonSound = true)
{
    audio_play_sound(snd_Button, 10, false);
    buttonSound = false;
}
if (hitSound = true)
{
    //audio_play_sound(snd_BlockHit, 10, false); too much cluster
    hitSound = false;
}
if (mergeSound = true)
{
    audio_play_sound(snd_Merge, 10, false);
    mergeSound = false;
}
if (swipeSound = true)
{
    audio_play_sound(snd_Swipe, 10, false);
    swipeSound = false;
}
if (levelCompleteSound = true)
{
    audio_play_sound(snd_LevelComplete, 10, false);
    levelCompleteSound = false;
}
if (menuSound = true)
{
    //audio_play_sound(snd_MenuOpen, 10, false); unnecessary
    menuSound = false;
}
if (restartSound = true)
{
    audio_play_sound(snd_levelRestart, 10, false);
    restartSound = false;
}

//Start Background Music
if (room = Levels || room = welcome)
{   
    if (obj_Logo.image_index = 0)
    {
        if (!audio_is_playing(snd_BGMusic))
        {
            audio_play_sound(snd_BGMusic, 1, true);
            BGMusicSound = true
            BGMusic2Sound = false
            BGMusic3Sound = false
        }
    }
    if (obj_Logo.image_index = 1)
    {
        if (!audio_is_playing(snd_BGMusic2))
        {
            audio_play_sound(snd_BGMusic2, 1, true);
            BGMusicSound = false 
            BGMusic2Sound = true
            BGMusic3Sound = false
        }
    } 
    if (obj_Logo.image_index = 2)
    {
        if (!audio_is_playing(snd_BGMusic3))
        {
            audio_play_sound(snd_BGMusic3, 1, true);
            BGMusicSound = false 
            BGMusic2Sound = false
            BGMusic3Sound = true
        }
    }    
}


//Stop Background Music
if (room != Levels && room != welcome)
{   
    if (audio_is_playing(snd_BGMusic) || audio_is_playing(snd_BGMusic2) || audio_is_playing(snd_BGMusic3)) 
    {
        BGMusicSound = false 
        BGMusic2Sound = false 
        BGMusic3Sound = false 

    }    
}

if (BGMusicSound = false )
{
    audio_stop_sound(snd_BGMusic);
}
if (BGMusic2Sound = false )
{
    audio_stop_sound(snd_BGMusic2);
}
if (BGMusic3Sound = false )
{
    audio_stop_sound(snd_BGMusic3);
}
