{
    //create particle
    global.pt_square= part_type_create();
    var pt = global.pt_square;
    
    //settings.
    part_type_shape(pt, pt_shape_square);
    part_type_size(pt,0.2, 0.5, 0, 0);
    part_type_color1(pt,c_white);
    part_type_speed(pt, 2, 3, 0, 0);
    part_type_direction(pt, 270,270, 0, 30);
    part_type_life(pt, room_speed*20, room_speed*20);
    part_system_depth(pt, -100000  )
     
}
